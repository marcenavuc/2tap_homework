from django.forms.models import model_to_dict
from django.db import models


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=1024, null=True)
    last_name = models.CharField(max_length=1024, null=True)
    username = models.CharField(max_length=1024, null=True)
    telephone = models.CharField(max_length=32, null=True)
    is_bot = models.BooleanField(default=False, null=True)

    @classmethod
    def from_dict(cls, params: dict):
        user_id = params['id']
        first_name = params.get('first_name')
        last_name = params.get('last_name')
        username = params.get('username')
        telephone = None
        is_bot = params.get('is_bot')
        return cls(user_id, first_name, last_name, username, telephone, is_bot)

    def to_dict(self):
        return model_to_dict(self)

    def get_beauty_desc(self):
        return f"""You name is {self.first_name} {self.last_name}
                   Your phone {self.telephone}
                   Your username {self.username}
                   You're bot {self.is_bot}"""

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    class Meta:
        verbose_name = 'User'
