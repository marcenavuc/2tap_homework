import re
import logging
from typing import Union

from app.internal.models.user import User


PHONE_REGEXP = re.compile(r'^(?:\+?44)?[07]\d{9,13}$')
logger = logging.getLogger(__name__)


class UserService:
    @staticmethod
    def set_phone(user_id: int, phone: str) -> Union[None, str]:
        logger.info(f"{user_id} {phone}")
        try:
            PHONE_REGEXP.match(phone).group(0)
            user = User.objects.get(id=user_id)
            user.telephone = phone
            user.save()
        except AttributeError as e:
            return 'phone is invalid'
        except Exception as e:
            logger.info(e)
            return 'User not found'

    @staticmethod
    def get_user(user_id: int) -> User:
        logger.info(user_id)
        try:
            user = User.objects.get(id=user_id)
            return user
        except Exception as e:
            return None

    @classmethod
    def get_user_data(cls, user_id: int) -> dict:
        user: User = cls.get_user(user_id=user_id)
        logger.info("user", user)
        return {} if user is None else user.to_dict()

    @classmethod
    def get_user_beauty_desc(cls, user_id: int) -> str:
        user: User = cls.get_user(user_id=user_id)
        return {} if user is None else user.get_beauty_desc()

    @staticmethod
    def save_user(message: dict) -> User:
        user = User.from_dict(message)
        user.save()
        return user
