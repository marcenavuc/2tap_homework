import json
import logging
from enum import Enum

import requests

from django.views import View
from django.http import JsonResponse
from django.conf import settings

from app.internal.services.user_service import UserService


logger = logging.getLogger(__name__)

TELEGRAM_URL = settings.TELEGRAM_URL
TUTORIAL_BOT_TOKEN = settings.TUTORIAL_BOT_TOKEN


class DialogState(Enum):
    DEFAULT = 1  # /me, /start
    SET_PHONE = 2  # /set_phone


class TelegramBotHandler(View):
    state = DialogState.DEFAULT

    def post(self, request, *args, **kwargs):
        t_data = json.loads(request.body)
        logger.info(t_data)
        t_message = t_data["message"]
        user_id = t_message['from']['id']
        chat_id = t_message['chat']['id']
        logger.debug(f"Current state {self.__class__.state}")
        try:
            text = t_message["text"].strip().lower()
        except Exception:
            return JsonResponse({"ok": "POST request processed"})

        if self.__class__.state == DialogState.DEFAULT:
            if text == '/start':
                UserService.save_user(t_message['from'])
                self.send_message("User successfully saved", chat_id)
            if text == "/me":
                result = UserService.get_user_beauty_desc(user_id)
                self.send_message(result, chat_id)
            if text == '/set_phone':
                self.__class__.state = DialogState.SET_PHONE
                self.send_message("Write your phone", chat_id)
        elif self.__class__.state == DialogState.SET_PHONE:
            message = UserService.set_phone(user_id=user_id, phone=text)
            logger.debug(message)
            if message is None:
                self.__class__.state = DialogState.DEFAULT
                self.send_message("Your phone saved ❤", chat_id)
            else:
                self.send_message(message, chat_id)

        return JsonResponse({"ok": "POST request processed"})

    @staticmethod
    def send_message(message, chat_id):
        data = {
            "chat_id": chat_id,
            "text": message,
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{TUTORIAL_BOT_TOKEN}/sendMessage", data=data
        )
        logger.info(response)
