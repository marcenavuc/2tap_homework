import logging

from ninja import Router

from app.internal.services.user_service import UserService

router = Router()
logger = logging.getLogger(__name__)

@router.post('/me')
def get_user_information(request, user_id: int) -> dict:
    logger.info(f"User id {user_id}")
    user_data: dict = UserService.get_user_data(user_id)
    return user_data

