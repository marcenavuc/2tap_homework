from ninja import NinjaAPI
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from app.internal.transport.bot.handlers import TelegramBotHandler
from app.internal.transport.rest.handlers import router

api = NinjaAPI()
api.add_router('/v1/', router)

urlpatterns = [
    path('bot/', csrf_exempt(TelegramBotHandler.as_view())),
    path('', api.urls)
]
